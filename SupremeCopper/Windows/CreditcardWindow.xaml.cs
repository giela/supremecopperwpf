﻿using System.Windows;
using SupremeCopper.Classes;

namespace SupremeCopper.Windows
{
    /// <summary>
    /// Interaction logic for CreditcardWindow.xaml
    /// </summary>
    public partial class CreditcardWindow : Window
    {
        public CreditcardWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreditcardInfo creditcardInfo = new CreditcardInfo(NameBox.Text, TypeBox.Text, NumberBox.Text, MonthBox.Text, YearBox.Text, CvvBox.Text);
            AddressAndCreditcard.CreditcardInfos.Add(creditcardInfo);
            AddressAndCreditcard.WriteCreditcardToLocalFile();
        }
    }
}
