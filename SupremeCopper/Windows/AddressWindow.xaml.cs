﻿using System.Windows;
using SupremeCopper.Classes;

namespace SupremeCopper.Windows
{
    /// <summary>
    /// Interaction logic for AddressWindow.xaml
    /// </summary>
    public partial class AddressWindow : Window
    {
        public AddressWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var personalInfo = new PersonalInfo(NameBox.Text, EmailBox.Text, TelBox.Text, AddressBox.Text, CityBox.Text, ZipBox.Text, CountryBox.Text);
            AddressAndCreditcard.PersonalInfos.Add(personalInfo);
            AddressAndCreditcard.WriteAddressToLocalFile();
        }
    }
}
