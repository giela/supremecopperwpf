﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using SupremeCopper.Classes;

namespace SupremeCopper.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        
        public MainWindow()
        {
            InitializeComponent();

            AddressAndCreditcard.ReadFromLocalFile();

            CreditCardBox.ItemsSource = AddressAndCreditcard.CreditcardInfos;
            AddressBox.ItemsSource = AddressAndCreditcard.PersonalInfos;
            TasksDataGrid.ItemsSource = Tasks.TasksCollection;

            CaptchaLabel.DataContext = SolvedCaptchas.SolvedCaptchasCollection.Count;
        }

        private void StartTaskButton_Click(object sender, RoutedEventArgs e)
        {
            List<Task> tasksToRun = TasksDataGrid.SelectedItems.Cast<Task>().ToList();
            foreach (Task task in tasksToRun)
            {
                task.Run();
            }
        }

        private void AddTaskButton_Click(object sender, RoutedEventArgs e)
        {
            int taskId = Tasks.TasksCollection.Count + 1;
            PersonalInfo address = (PersonalInfo)AddressBox.SelectedItem;
            CreditcardInfo creditcard = (CreditcardInfo)CreditCardBox.SelectedItem;
            int refreshDelay = 0;
            int checkoutDelay = 0;

            if (int.TryParse(RefreshDelayBox.Text, out refreshDelay) &&
                int.TryParse(CheckoutDelayBox.Text, out checkoutDelay))
            {
                Task task = new Task(taskId, CategoryBox.Text, KeywordBox.Text, ColorsAndSizes.Colors.ToList(), ColorsAndSizes.Sizes.ToList(), refreshDelay,
                    checkoutDelay, address, creditcard, ProxyBox.Text);
                Tasks.TasksCollection.Add(task);
            }
            else
            {
                //TODO: Add event for when textboxes are not ints
            }
        }

        private void ColorButton_Click(object sender, RoutedEventArgs e)
        {
            ColorWindow colorWindow = new ColorWindow();
            colorWindow.Show();
        }

        private void SizeButton_Click(object sender, RoutedEventArgs e)
        {
            SizeWindow sizeWindow = new SizeWindow();
            sizeWindow.Show();
        }

        private void NewAddress_Click(object sender, RoutedEventArgs e)
        {
            Windows.AddressWindow addressWindow = new Windows.AddressWindow();
            addressWindow.Show();
        }

        private void NewCreditcard_Click(object sender, RoutedEventArgs e)
        {
            CreditcardWindow creditcardWindow = new CreditcardWindow();
            creditcardWindow.Show();
        }

        private void CaptchaSolver_Click(object sender, RoutedEventArgs e)
        {
            CaptchaSolverWindow captchasolverWindow = new CaptchaSolverWindow();
            captchasolverWindow.Show();
        }

        private void RemoveTaskButton_OnClick(object sender, RoutedEventArgs e)
        {
            List<Task> tasksToRun = TasksDataGrid.SelectedItems.Cast<Task>().ToList();
            foreach (Task task in tasksToRun)
            {
                Tasks.TasksCollection.Remove(task);
            }
        }
    }
}
