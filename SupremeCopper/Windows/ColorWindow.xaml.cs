﻿using System.Windows;
using SupremeCopper.Classes;

namespace SupremeCopper.Windows
{
    /// <summary>
    /// Interaction logic for ColorWindow.xaml
    /// </summary>
    public partial class ColorWindow : Window
    {


        public ColorWindow()
        {
            InitializeComponent();
            PrefferedColorsBox.ItemsSource = ColorsAndSizes.Colors;
        }

        private void AddColorButton_Click(object sender, RoutedEventArgs e)
        {
            ColorsAndSizes.Colors.Add(ColorBox.Text);
        }

        private void RemoveColorButton_Click(object sender, RoutedEventArgs e)
        {
            if (PrefferedColorsBox.SelectedItem != null)
            {
                ColorsAndSizes.Colors.Remove(PrefferedColorsBox.SelectedItem.ToString());
            }
        }
    }
}
