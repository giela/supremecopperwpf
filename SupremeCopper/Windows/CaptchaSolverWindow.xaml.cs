﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;
using SupremeCopper.Classes;

namespace SupremeCopper.Windows
{
    /// <summary>
    /// Interaction logic for CaptchaSolverWindow.xaml
    /// </summary>
    public partial class CaptchaSolverWindow : Window
    {
        public CaptchaSolverWindow()
        {
            InitializeComponent();
            CaptchaDataGrid.ItemsSource = SolvedCaptchas.SolvedCaptchasCollection;
        }

        private void Hyperlink_OnRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void SolveCaptchaButton_Click(object sender, RoutedEventArgs e)
        {
            int amount = 0;
            if (int.TryParse(AmountBox.Text, out amount))
            {
                SolvedCaptchas.SolveCaptchas(ApiKeyBox.Text, SiteKeyBox.Text, amount, ProyBox.Text, PageUrlBox.Text, ProxyTypeBox.Text);
            }
        }

        private void SolveCaptchaForTasksButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (Task task in Tasks.TasksCollection)
            {
                SolvedCaptchas.SolveCaptchas(ApiKeyBox.Text, SiteKeyBox.Text, 1, task.Proxy, PageUrlBox.Text);
            }
        }
    }
}
