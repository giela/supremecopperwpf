﻿using System.Windows;
using SupremeCopper.Classes;

namespace SupremeCopper.Windows
{
    /// <summary>
    /// Interaction logic for ColorWindow.xaml
    /// </summary>
    public partial class SizeWindow : Window
    {
        public SizeWindow()
        {
            InitializeComponent();
            PrefferedSizesBox.ItemsSource = ColorsAndSizes.Sizes;
        }

        private void AddSizeButton_Click(object sender, RoutedEventArgs e)
        {
            ColorsAndSizes.Sizes.Add(SizeBox.Text);
        }

        private void RemoveSizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (PrefferedSizesBox.SelectedItem != null)
            {
                ColorsAndSizes.Sizes.Remove(PrefferedSizesBox.SelectedItem.ToString());
            }
        }
    }
}
