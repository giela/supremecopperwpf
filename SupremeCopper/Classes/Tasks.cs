﻿using System.Collections.ObjectModel;

namespace SupremeCopper.Classes
{
    public static class Tasks
    {
        public static ObservableCollection<Task> TasksCollection { get; set; }
        static  Tasks()
        {
            TasksCollection = new ObservableCollection<Task>();
        }
    }
}
