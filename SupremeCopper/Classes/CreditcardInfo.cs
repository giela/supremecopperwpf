﻿using System;

namespace SupremeCopper.Classes
{
    public class CreditcardInfo
    {
        public string Name { get; }
        public string Type { get; }
        public string Number { get; }
        public string Month { get; }
        public string Year { get; }
        public string Cvv { get; }
        public string LastDigits { get; }

        public CreditcardInfo(string name, string type, string number, string month, string year, string cvv)
        {
            Name = name;
            Type = type;
            Number = number;
            Month = month;
            Year = year;
            Cvv = cvv;
            var stars = "";
            for (var i = 0; i < (number.Length - 5); i++)
            {
                stars += "*";
            }
            LastDigits = stars + number.Substring(Math.Max(0, number.Length - 5));
        }
    }
}
