﻿namespace SupremeCopper.Classes
{
    public class PersonalInfo
    {
        public string Name { get; }
        public string Email { get; }
        public string Tel { get; }
        public string Address { get; }
        public string City { get; }
        public string Zip { get; }
        public string Country { get; }

        public PersonalInfo(string name, string email, string tel, string address, string city, string zip, string country)
        {
            Name = name;
            Email = email;
            Tel = tel;
            Address = address;
            City = city;
            Zip = zip;
            Country = country;
        }
    }
}
