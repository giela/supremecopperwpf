﻿using System;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.Windows;
using OpenQA.Selenium.Chrome;

namespace SupremeCopper.Classes
{
    public static class SolvedCaptchas 
    {
        public static ObservableCollection<SolvedCaptcha> SolvedCaptchasCollection { get; set; }
        

        static SolvedCaptchas()
        {
            SolvedCaptchasCollection = new ObservableCollection<SolvedCaptcha>();
        }

        //TODO: Make us of a different technique like a json reader or an rss reader not selenium webdriver.
        /// <summary>
        /// Solve set amount of captchas and add the to observableCollection 
        /// </summary>
        /// <param name="apikey">The 2captcha apikey</param>
        /// <param name="sitekey">Sitekey from captcha you want to solve</param>
        /// <param name="amount">Amount of captchas should be solved</param>
        /// <param name="proxy">Proxy 2captcha should use when solving captchas</param>
        /// <param name="proxytype">proxy type for used proxy</param>
        /// <param name="pageurl">Pageurl of the captcha</param>
        public static void SolveCaptchas(string apikey, string sitekey, int amount, string proxy,string pageurl, string proxytype = "HTTP")
        {
            ObservableCollection<SolvedCaptcha> solvedCaptchaCollection= new ObservableCollection<SolvedCaptcha>();
            //TODO: THREADS AND SHIT https://stackoverflow.com/questions/25414964/label-changed-in-while-loop-does-not-update-ui optimize it
            System.Threading.Tasks.Task.Run(() =>
            {
                ChromeDriver driver = new ChromeDriver();
                driver.Manage().Window.Size = new System.Drawing.Size(1440, 1000);
                string postUrl;
                //Url for post request on 2captcha
                if (proxy.Length <= 0)
                {
                    postUrl = "http://2captcha.com/in.php?key=" + apikey + "&method=userrecaptcha&googlekey=" +
                              sitekey +
                              "&pageurl=" + pageurl + "&json=1";
                }
                else
                {
                    postUrl = "http://2captcha.com/in.php?key=" + apikey + "&method=userrecaptcha&googlekey=" +
                              sitekey +
                              "&pageurl=" + pageurl + "&json=1&proxy=" + proxy + "&proxytype="+ proxytype;
                }
                // Get all capthcaids
                for (var i = 0; i < amount; i++)
                {
                    SolvedCaptcha solvedCaptcha = new SolvedCaptcha();
                    solvedCaptcha.Proxy = proxy;
                    driver.Navigate().GoToUrl(postUrl);
                    string json = driver.FindElementByTagName("body").Text;
                    JObject jsonObject = JObject.Parse(json);
                    if (int.Parse(jsonObject["status"].ToString()) == 1)
                    {
                        solvedCaptcha.Captchaid = jsonObject["request"].ToString();
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        solvedCaptchaCollection.Add(solvedCaptcha);
                    });
                }

                int isComplete = 0;
                while (isComplete != amount)
                {
                    isComplete = 0;
                    // Get all captcha tokens
                    foreach (SolvedCaptcha solvedCaptcha in solvedCaptchaCollection)
                    {
                        if (solvedCaptcha.Status)
                        {
                            ++isComplete;
                        }
                        else
                        {
                            string url = "http://2captcha.com/res.php?key=" + apikey + "&action=get&id=" +
                                         solvedCaptcha.Captchaid +
                                         "&json=1";
                            driver.Navigate().GoToUrl(url);
                            string json = driver.FindElementByTagName("body").Text;
                            JObject jsonObject = JObject.Parse(json);
                            if (int.Parse(jsonObject["status"].ToString()) == 1)
                            {
                                solvedCaptcha.Captchatoken = jsonObject["request"].ToString();
                                solvedCaptcha.SetExpireTime();
                                solvedCaptcha.Status = true;
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    SolvedCaptchasCollection.Add(solvedCaptcha);
                                });
                                ++isComplete;
                            }
                        }
                    }
                    Tools.WaitMs(TimeSpan.FromMilliseconds(5000),driver);
                }

                driver.Quit();
            });
        }
    }
}
