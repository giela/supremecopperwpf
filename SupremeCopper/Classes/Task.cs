﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SupremeCopper.Classes
{
    public class Task
    {
        public int TaskId { get; }
        public string Category { get; }
        public string KeyWord { get; }
        public List<string> Colors { get; }
        public List<string> Size { get; }
        public int RefreshDelay { get; }
        public int CheckoutDelay { get; }
        public PersonalInfo PersonalInfo { get; }
        public CreditcardInfo CreditcardInfo { get; }
        public string Proxy { get; }

        public Task(int taskId, string category, string keyWord, List<string> color, List<string> size, int refreshDelay, int checkoutDelay, PersonalInfo personalInfo, CreditcardInfo creditcardInfo, string proxy)
        {
            Category = category;
            KeyWord = keyWord.ToLower();
            Colors = color.ConvertAll(c => c.ToLower());
            Size = size.ConvertAll(s => s.ToLower());
            for (var i = 0; i < Size.Count; i++)
            {
                switch (Size[i])
                {
                    case "small":
                        Size[i] = "Small";
                        break;
                    case "medium":
                        Size[i] = "Medium";
                        break;
                    case "large":
                        Size[i] = "Large";
                        break;
                    case "xlarge":
                        Size[i] = "XLarge";
                        break;
                }
            }

            RefreshDelay = refreshDelay;
            CheckoutDelay = checkoutDelay;
            PersonalInfo = personalInfo;
            CreditcardInfo = creditcardInfo;
            Proxy = proxy;
            TaskId = taskId;
        }

        /// <summary>
        /// Try to cop an item
        /// </summary>
        public void Run()
        {        
            System.Threading.Tasks.Task.Run(() =>
            {
                //intialize proxy
                Proxy proxy = new Proxy
                {
                    FtpProxy = Proxy,
                    HttpProxy = Proxy,
                    SslProxy = Proxy
                };
                ChromeOptions options = new ChromeOptions
                {
                    Proxy = proxy
                };


                //start driver

                ChromeDriver driver = Proxy.Length == 0 ? new ChromeDriver() : new ChromeDriver(options);

                driver.Manage().Window.Size = new System.Drawing.Size(1440, 1000);
                driver.Navigate().GoToUrl("http://www.supremenewyork.com/shop/all/" + Category);
                
                BuyProduct(driver);
                //Uncomment this when done with development
                //driver.Quit();
            });
        }

        //TODO Test this
        private void BuyProduct(ChromeDriver driver)
        {
            string itemUrl = null;
            GetItemUrl();
            AddToCart();
            Checkout();
            
            void GetItemUrl(Boolean refresh = false)
            {
                if (refresh)
                {
                    //refresh delay
                    Tools.WaitMs(TimeSpan.FromMilliseconds(RefreshDelay), driver);
                    //refresh page
                    driver.Navigate().Refresh();
                }

                ReadOnlyCollection<IWebElement> itemNames = driver.FindElementsByCssSelector("article div.inner-article h1 a.name-link");
                //check if user cares about color or not and then select item appropriately.
                if (Colors.Count > 0)
                {
                    ReadOnlyCollection<IWebElement> itemColors =
                        driver.FindElementsByCssSelector("article div.inner-article p a.name-link");

                    for (var j = 0; j < Colors.Count; j++)
                    {
                        for (var i = 0; i < itemNames.Count; i++)
                        {
                            var nameString = itemNames[i].Text.ToLower();
                            if (nameString.Contains(KeyWord))
                            {
                                var colorString = itemColors[i].Text.ToLower();
                                if (colorString.Contains(Colors[j]))
                                {
                                    itemUrl = itemNames[i].GetAttribute("href");
                                    j = Colors.Count + 1;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var itemName in itemNames)
                    {
                        var nameString = itemName.Text.ToLower();
                        if (nameString.Contains(KeyWord))
                        {
                            itemUrl = itemName.GetAttribute("href");
                            break;
                        }
                    }
                }

                //if not itemfound refresh page and try and find it again.
                if (itemUrl == null)
                {
                    GetItemUrl(true);
                }
            }

            void AddToCart(){
                driver.Navigate().GoToUrl(itemUrl);
                //try and select size if dropdown element exists else just add to cart.
                try
                {
                    IWebElement sizElement = driver.FindElementByCssSelector("select#size");
                    SelectElement selectElement = new SelectElement(sizElement);
                    //select size
                    for (var i = 0; i < Size.Count; i++)
                    {
                        for (var j = 0; j < selectElement.Options.Count; j++)
                        {
                            if (selectElement.Options[j].Text.ToLower() == Size[i])
                            {
                                selectElement.SelectByIndex(j);
                                //selectElement.SelectByText(Size[i]);
                                i = Size.Count + 1;
                                break;
                            }
                            else if (i == Size.Count)
                            {
                                Console.WriteLine("Size is not avalaible");
                            }

                        }
                    }
                }
                catch (NoSuchElementException)
                {
                }

                driver.FindElementByXPath("//*[@id='add-remove-buttons']/input").Click();
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                try
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='cart']")));
                    driver.FindElementByXPath("//*[@id='cart']/a[2]").Click();
                    try
                    {
                        wait.Until(ExpectedConditions.ElementExists(By.XPath("//*[@id='checkout_form']")));
                        //maybe I can run checkout from here? might not be usefull when having multiple items per task
                    }
                    catch (TimeoutException)
                    {
                        Console.WriteLine("Checkout does not open");
                    }
                }
                catch (TimeoutException)
                {
                    Console.WriteLine("Cart is not showing");
                }

            }

            void Checkout()
            {
                //Personal info credit card name used because this has to be equal to the credit card used
                SelectElement countrySelectElement = new SelectElement(driver.FindElementById("order_billing_country"));
                countrySelectElement.SelectByText(PersonalInfo.Country.ToUpper());
                driver.FindElementById("order_billing_name").SendKeys(CreditcardInfo.Name);
                driver.FindElementById("order_email").SendKeys(PersonalInfo.Email);
                driver.FindElementById("order_tel").SendKeys(PersonalInfo.Tel);
                driver.FindElementById("bo").SendKeys(PersonalInfo.Address);
                driver.FindElementById("order_billing_city").SendKeys(PersonalInfo.City);
                driver.FindElementById("order_billing_zip").SendKeys(PersonalInfo.Zip);

                //Credit card info
                SelectElement creditcardSelectElement = new SelectElement(driver.FindElementById("credit_card_type"));
                creditcardSelectElement.SelectByText(CreditcardInfo.Type);
                //driver.FindElementById("cnb").SendKeys(CreditcardInfo.Number);
                if (CreditcardInfo.Type != "PayPal")
                {
                    driver.ExecuteScript("document.getElementById('cnb').value = '" + CreditcardInfo.Number + "'");
                    driver.FindElementById("credit_card_month").SendKeys(CreditcardInfo.Month);
                    driver.FindElementById("credit_card_year").SendKeys(CreditcardInfo.Year);
                    driver.FindElementById("vval").SendKeys(CreditcardInfo.Cvv);
                }
                //click the check mark for terms and services
                driver.FindElementsByClassName("icheckbox_minimal")[1].Click();

                //solve captcha and pick captcha with correct proxy + status

                string captchaToken = "empty";
                for (int i = 0; i < SolvedCaptchas.SolvedCaptchasCollection.Count; i++)
                {
                    if ((SolvedCaptchas.SolvedCaptchasCollection[i].Status &&
                         SolvedCaptchas.SolvedCaptchasCollection[i].Proxy == Proxy) || (SolvedCaptchas.SolvedCaptchasCollection[i].Status && Proxy.Length == 0))
                    {
                        captchaToken = SolvedCaptchas.SolvedCaptchasCollection[i].Captchatoken;
                        SolvedCaptchas.SolvedCaptchasCollection[i].RemoveFromCollection();
                        i = SolvedCaptchas.SolvedCaptchasCollection.Count + 1;
                    }
                }
                //makes it possible for manual captcha solving
                if (captchaToken != "empty")
                {

                    driver.ExecuteScript("document.getElementById('g-recaptcha-response').innerHTML = '" +
                                         captchaToken + "';");
                }
                //checkout delay
                Tools.WaitMs(TimeSpan.FromMilliseconds(CheckoutDelay), driver);

                string totalprice = driver.FindElementByXPath("//*[@id='total']").Text;
                try
                {
                    if (Convert.ToDecimal(totalprice) > 1000)
                    {
                        Console.WriteLine("total price = " + totalprice);
                    }
                    else
                    {
                        Console.WriteLine(totalprice);
                        driver.FindElementByCssSelector("input.button.checkout").Click();
                        if (CreditcardInfo.Type == "PayPal")
                        {
                            PayPal();
                        }
                        //driver.ExecuteScript("document.getElementById('checkout_form').submit();");
                    }
                }
                catch (Exception e)
                {
                    driver.FindElementByCssSelector("input.button.checkout").Click();
                    if (CreditcardInfo.Type == "PayPal")
                    {
                        PayPal();
                    }
                    //driver.ExecuteScript("document.getElementById('checkout_form').submit();");
                    Console.WriteLine(e);
                }
                Console.WriteLine("Item copped");
            }

            //method for buying item(s) with paypal
            void PayPal()
            {
                //user has a chance of manually solving catpcha
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Tools.ManualFriendlyWait));
                try
                {
                    wait.Until(ExpectedConditions.ElementExists(By.XPath("//*[@id='loginSection']/div/div[2]/a"))); 
                    driver.FindElementByXPath("//*[@id='loginSection']/div/div[2]/a").Click();
                    try
                    {
                        wait.Until(ExpectedConditions.ElementExists(By.XPath("//*[@id='passwordSection']")));
                        driver.FindElementById("email").SendKeys(CreditcardInfo.Number);
                        driver.FindElementById("password").SendKeys(CreditcardInfo.Cvv);
                        driver.FindElementById("btnLogin").Click();
                        wait.Until(ExpectedConditions.ElementExists(By.Id("btnSelectSoftToken")));
                        driver.FindElementById("btnSelectSoftToken").Click();
                    }
                    catch (TimeoutException)
                    {
                        Console.WriteLine("Paypal was too slow to load");
                    }
                }
                catch (TimeoutException)
                {
                    Console.WriteLine("Paypal won't load");
                }

            }
        }
    }
}
