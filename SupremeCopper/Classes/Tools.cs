﻿using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SupremeCopper.Classes
{
    public static class Tools
    {
        public static readonly int ManualFriendlyWait = 80;
        public static void WaitMs(TimeSpan timeToWait, ChromeDriver driver)
        {
            var now = DateTime.Now;
            var wait = new WebDriverWait(driver, timeToWait);
            wait.PollingInterval = TimeSpan.FromMilliseconds(100);
            wait.Until(wd => (DateTime.Now - now) - timeToWait > TimeSpan.Zero);
        }
    }
}
