﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace SupremeCopper.Classes
{
    public static class AddressAndCreditcard
    {
        public static ObservableCollection<PersonalInfo> PersonalInfos { get; set; }
        public static ObservableCollection<CreditcardInfo> CreditcardInfos { get; set; }
        private const string DirectoryName = @"C:\SupremeCopper";

        static AddressAndCreditcard()
        {
            PersonalInfos = new ObservableCollection<PersonalInfo>();
            CreditcardInfos = new ObservableCollection<CreditcardInfo>();
            Directory.CreateDirectory(DirectoryName);
        }

        /// <summary>
        /// Write to json file
        /// </summary>
        public static void WriteAddressToLocalFile()
        {
            const string path = DirectoryName + @"\Address.json";
            var json = JsonConvert.SerializeObject(PersonalInfos.ToList());
            File.WriteAllText(path, json);
        }

        /// <summary>
        /// Write to json file
        /// </summary>
        public static void WriteCreditcardToLocalFile()
        {
            const string path = DirectoryName + @"\Creditcard.json";
            var json = JsonConvert.SerializeObject(CreditcardInfos.ToList());
            File.WriteAllText(path, json);
        }

        /// <summary>
        /// Read json from local file and set it to object
        /// </summary>
        public static void ReadFromLocalFile()
        {
            if (File.Exists(DirectoryName + @"\Address.json"))
            {
                var jsonAddress = File.ReadAllText(DirectoryName + @"\Address.json");
                var personalInfoList = JsonConvert.DeserializeObject<List<PersonalInfo>>(jsonAddress);
                PersonalInfos = new ObservableCollection<PersonalInfo>(personalInfoList);
            }

            if (File.Exists(DirectoryName + @"\Creditcard.json"))
            {
                var jsonCreditcard = File.ReadAllText(DirectoryName + @"\Creditcard.json");
                var creditcardInfoList =
                    JsonConvert.DeserializeObject<List<CreditcardInfo>>(jsonCreditcard);
                CreditcardInfos = new ObservableCollection<CreditcardInfo>(creditcardInfoList);
            }
        }

    }
}
