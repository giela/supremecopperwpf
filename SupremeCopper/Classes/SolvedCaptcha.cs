﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Windows;

namespace SupremeCopper.Classes
{
    public class SolvedCaptcha : INotifyPropertyChanged
    {
        private string _captchatoken;
        public string Captchaid { get; set; }
        public string Captchatoken { get { return _captchatoken; } set { _captchatoken = value; NotifyPropertyChanged(); } } 
        public TimeSpan ExpireTime { get; private set; }
        public string Proxy { get; set; }
        public bool Status { get; set; }
        private readonly Timer _timer = new Timer();

        public event PropertyChangedEventHandler PropertyChanged;

        public SolvedCaptcha(string captchaid, string captchatoken)
        {
            Captchaid = captchaid;
            Captchatoken = captchatoken;
            ExpireTime = new TimeSpan();
        }

        public SolvedCaptcha()
        {
            Status = false;
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void SetExpireTime()
        {
            ExpireTime = TimeSpan.FromSeconds(115);
            _timer.Interval = 1000;
            _timer.Elapsed += Timer_Tick;
            _timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (ExpireTime.TotalSeconds > 0)
            {
                ExpireTime = ExpireTime.Subtract(TimeSpan.FromSeconds(1));
                NotifyPropertyChanged("ExpireTime");
            }
            else
            {
                _timer.Stop();
                RemoveFromCollection();
            }
        }

        public void RemoveFromCollection()
        {
            // Used this https://stackoverflow.com/a/7244520 for this problem: "This type of CollectionView does not support changes to its  SourceCollection from a thread different from the Dispatcher thread."
            Application.Current.Dispatcher.Invoke(() =>
            {
                SolvedCaptchas.SolvedCaptchasCollection.Remove(this);
            });
        }
    }
}
