﻿using System.Collections.ObjectModel;

namespace SupremeCopper.Classes
{
    public static class ColorsAndSizes
    {
        public static ObservableCollection<string> Colors { get; set; }
        public static ObservableCollection<string> Sizes { get; set; }

        static ColorsAndSizes()
        {
            Colors = new ObservableCollection<string>();
            Sizes = new ObservableCollection<string>();
        }
    }
}
